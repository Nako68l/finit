import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideBarComponent } from './side-bar/side-bar.component';
import { RouterModule } from '@angular/router';
import { ListMenuComponent } from './side-bar/list-menu/list-menu.component';

@NgModule({
  declarations: [
    SideBarComponent,
    ListMenuComponent,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    SideBarComponent
  ]
})
export class ComponentsModule { }
