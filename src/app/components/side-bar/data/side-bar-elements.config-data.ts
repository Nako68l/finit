import { Item } from "../models/item.model";

export const items: Item[] = [
    {
        title: 'Users', children: [
            { title: 'Report sum by user', link: '/users/reportByUser' },
            { title: 'Something else', link: '/login' }
        ]
    },
    {
        title: 'Something', children: [
            { title: 'asdkjasldkj', link: '/somewhere' },
            { title: 'asdlka;sldk', link: '/login' }
        ]
    },
    { title: 'Else' }
]