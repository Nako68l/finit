import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from '../models/item.model';

@Component({
  selector: 'app-list-menu',
  templateUrl: './list-menu.component.html',
  styleUrls: ['./list-menu.component.scss']
})
export class ListMenuComponent implements OnInit {
  @Input() items: Item[];

  selectedItem: Item;

  constructor(
    private route: Router
  ) { }

  ngOnInit() {
    this.selectedItem = this.getSelectedItem(this.items)
  }

  getSelectedItem = (items: Item[]) => this.items.find(item => item.children ? this.childHaveRoute(item.children) : false)

  private childHaveRoute = (children) => !!children.find(child => child.link === this.route.url ? true : false)

  toggleItem = (item) => this.selectedItem === item ? this.selectedItem = null : this.selectedItem = item;
}
