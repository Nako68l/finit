import { Component, OnInit } from '@angular/core';
import { items } from "./data/side-bar-elements.config-data";
import { Item } from './models/item.model';
import { LoginService } from '@services/login/login.service';
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  items: Item[] = items

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    console.log(items);
  }

  onLogout(){
    this.loginService.logout()
  }

}
