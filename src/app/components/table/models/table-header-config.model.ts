export interface TableHeaderConfig {
    [index: number]: {
        title: string;
        field: string
    },
}