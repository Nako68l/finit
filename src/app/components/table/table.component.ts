import { Component, OnInit, Input } from '@angular/core';
import { UserReportInfo } from 'app/services/report/report.service';
import { ascSort, descSort } from '@shared/functions.service';
import { TableHeaderConfig } from './models/table-header-config.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() usersReportInfo: UserReportInfo[];
  @Input() tableHeaderConfig: TableHeaderConfig;
  asc: boolean;
  sortingProp: string;
  searchName;

  constructor() { }

  ngOnInit() {
  }

  sort(prop: string) {
    this.sortingProp === prop ?
      this.asc = !this.asc :
      this.asc = true

    this.sortingProp = prop;
    this.asc ?
      this.usersReportInfo = ascSort(this.usersReportInfo, prop) :
      this.usersReportInfo = descSort(this.usersReportInfo, prop)
  }
}
