import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { emailRegEx } from '@shared/data.service';
import { LoginService } from 'app/services/login/login.service';
import { Router } from '@angular/router';
import { Observable, empty } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginError: string;

  constructor(
    private fb: FormBuilder,
    public loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.pattern(emailRegEx),
      ]],
      password: ['']
    })
  }

  onSubmit() {
    this.loginError = '';
    this.loginService.login(this.email, this.password).pipe(
      catchError(httpError => {
        this.loginError = httpError.error.error_description
        return empty()
      }))
      .subscribe(r => {
        this.router.navigate(['/users/reportByUser'])
      })
  }

  get email() {
    return this.loginForm.get('email').value
  }

  get password() {
    return this.loginForm.get('password').value
  }
}
