import { TableHeaderConfig } from "app/components/table/models/table-header-config.model";

export const tableHeaderConfigData: TableHeaderConfig  = [
    { title: 'Display name', field: 'displayName' },
    { title: 'Round Half Time Spent', field: 'roundTimeSpent' },
    { title: 'Round Time Spent', field: 'roundHalfTimeSpent' },
    { title: 'Salary GRN Brutto', field: 'salaryGrnBrutto' },
    { title: 'Salary GRN Netto', field: 'salaryGrnNetto' },
    { title: 'Salary USD Brutto', field: 'salaryUsdBrutto' },
    { title: 'Salary USD Netto', field: 'salaryUsdNetto' },
    { title: 'Time Spent', field: 'timeSpent' },
  ]