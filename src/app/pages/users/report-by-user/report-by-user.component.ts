import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ReportService, UserReportInfo } from 'app/services/report/report.service';
import { Observable, from } from 'rxjs';
import { TableHeaderConfig } from 'app/components/table/models/table-header-config.model';
import { DateRangeValidators } from '@shared/date-range-validator/date-range-validator';
import {tableHeaderConfigData } from './data/table-header.config-data'
@Component({
  selector: 'app-report-by-user',
  templateUrl: './report-by-user.component.html',
  styleUrls: ['./report-by-user.component.scss'],
  providers: [DatePipe]
})
export class ReportByUserComponent implements OnInit {
  private dateForm: FormGroup;
  usersReportInfo: UserReportInfo[] = [];

  tableHeaderConfig: TableHeaderConfig = tableHeaderConfigData;

  constructor(
    private fb: FormBuilder,
    private pipe: DatePipe,
    private reportService: ReportService
  ) { }

  ngOnInit() {
    this.dateForm = this.fb.group({
      dateStart: null,
      dateFinish: null,
    }, {
        validator: DateRangeValidators.dateLessThan('dateFinish', 'dateStart'),
      })
    this.initFormValues();
    this.reportService.getUsersReportByPeriod(this.dateForm.value).subscribe(r => {
      this.usersReportInfo = r
    })
  }

  initFormValues() {
    const currentDate = new Date();
    const monthAgo = new Date(currentDate);
    monthAgo.setMonth(currentDate.getMonth() - 1)

    this.dateForm.setValue({ dateStart: this.pipe.transform(monthAgo, 'yyyy-MM-dd'), dateFinish: this.pipe.transform(currentDate, 'yyyy-MM-dd') })
  }

  onChange(e) {
    this.reportService.getUsersReportByPeriod(this.dateForm.value).subscribe(r => {
      this.usersReportInfo = r;
    })
  }

  get dateStartControl() {
    return this.dateForm.get('dateStart')
  }

  get dateFinishControl() {
    return this.dateForm.get('dateFinish')
  }

  get dateStart() {
    return this.dateForm.get('dateStart').value
  }

  get dateFinish() {
    return this.dateForm.get('dateFinish').value
  }

}
