import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportByUserComponent } from './report-by-user.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableModule } from 'app/components/table/table.module';

const routes: Routes = [
  {
    path: '', component: ReportByUserComponent
  }
]

@NgModule({
  declarations: [ReportByUserComponent],
  imports: [
    TableModule,
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ]
})
export class ReportByUserModule { }
