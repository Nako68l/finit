import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { config } from '@common/endpoints.config.js';
import { Observable, throwError } from 'rxjs';
import { LoginService, UserTokenInfo } from './login/login.service';
import { tap, concatMapTo, delay, concatMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
interface Options {
  headers?: {
    name: string,
    value: any
  },
  params?: {
    name: string,
    value: any
  }
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private options: Options = {};

  constructor(
    private http: HttpClient,
    private loginService: LoginService,
    private toastr: ToastrService
  ) {
  }

  authUser() {
    return this.loginService.getUser().pipe(
      tap((userInfo: UserTokenInfo) => {
        this.addHeaders({ Authorization: 'Bearer ' + userInfo.access_token })
      })
    )
  }

  post<T>(url, body, options?): Observable<T> {
    return this.authUser().pipe(
      tap(() => this.addOptions(options)),
      concatMap(() => this.http.post<T>(environment.backendUrl + config[url], body, { ...this.options })),
      catchError(this.errorResolver)
    )
  }

  get<T>(url, options?): Observable<T> {
    return this.authUser().pipe(
      tap(() => this.addOptions(options)),
      concatMap(() => this.http.get<T>(environment.backendUrl + config[url], { ...this.options })),
      catchError(this.errorResolver)
    )
  }

  addOptions(options) {
    this.options = { ...this.options, ...options }
  }

  addHeaders(headers) {
    this.options.headers = { ...this.options.headers, ...headers }
  }

  addParams(params) {
    this.options.params = { ...this.options.params, ...params }
  }

  errorResolver = (httpError) => {
      this.toastr.error(httpError.message,'Error')
      return throwError(httpError)
  }
}
