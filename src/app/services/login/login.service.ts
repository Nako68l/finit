import { Injectable } from '@angular/core';
import { tap, catchError } from 'rxjs/operators'
import { StorageService } from '../sotrage/storage.service';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { config } from '@common/endpoints.config.js';
export interface UserTokenInfo {
  access_token: string
  token_type: string,
  refresh_token: string,
  expires_in: number,
  scope: string,
  receive_time?: Date
}
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  userInfo: UserTokenInfo;
  userInfoStorageKey = 'userInfo'
  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private router: Router
  ) {
  }

  login(username, password) {
    const formData = new FormData();
    formData.set('username', username);
    formData.set('password', password);
    formData.set('grant_type', 'password');
    return this.loginUser(formData);
  }

  private loginUser(formData): Observable<UserTokenInfo> {
    return this.http.post<UserTokenInfo>(environment.backendUrl + config['login'], formData, { headers: { Authorization: 'Basic ' + btoa('fin-client:fin-secret') } }).pipe(
      tap((userInfo: UserTokenInfo) => {
        this.storage.setItem(this.userInfoStorageKey, { ...userInfo, receive_time: new Date() })
      })
    )
  }

  getUser(): Observable<UserTokenInfo> {
    this.userInfo = this.storage.getItem(this.userInfoStorageKey)
    if (this.userInfo) {
      return this.isTokenFresh(this.userInfo) ?
        of<UserTokenInfo>(this.userInfo) :
        this.refreshToken(this.userInfo.refresh_token)
    } else {
      this.router.navigate(['/login'])
    }
  }

  private isTokenFresh(userInfo: UserTokenInfo): boolean {
    const currentDate = new Date()
    const dateExpires = new Date(userInfo.receive_time)
    dateExpires.setSeconds(dateExpires.getSeconds() + userInfo.expires_in)
    console.log(currentDate, dateExpires, dateExpires >= currentDate);
    return dateExpires >= currentDate
  }

  private refreshToken(refresh_token) {
    const formData = new FormData();
    formData.set('refresh_token', refresh_token);
    formData.set('grant_type', 'refresh_token');
    return this.loginUser(formData);
  }

  logout() {
    this.storage.removeItem(this.userInfoStorageKey)
    this.router.navigate(['/login'])
  }
}
