import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';

export interface UserReportInfo {
  displayName: string
  project: string
  roundTimeSpent: number
  roundHalfTimeSpent: number
  salaryGrnBrutto: string
  salaryGrnNetto: string
  salaryUsdBrutto: string
  salaryUsdNetto: string
  timeSpent: number
}@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(
    private api: ApiService
  ) { }

  getUsersReportByPeriod(dates): Observable<UserReportInfo[]> {
    return this.api.get<UserReportInfo[]>('usersReportByPeriod', { params: dates })
  }
}
