import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setItem(key, item) {
    localStorage.setItem(key, JSON.stringify(item))
  }

  getItem(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  removeItem(key) {
    localStorage.removeItem(key)
  }
}
