import { ValidatorFn, AbstractControl } from "@angular/forms";

export class DateRangeValidators {
    static dateLessThan(dateField1: string, dateField2: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const date1 = control.get(dateField1).value;
            const date2 = control.get(dateField2).value;
            const dateIsLessThan = (date1 !== null && date2 !== null) && date1 < date2;
            const message = { 'dateLessThan': { value: date2 } };
            return dateIsLessThan ? message : null;
         };
    }
}
