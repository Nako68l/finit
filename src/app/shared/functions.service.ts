import * as R from "ramda";

export const ascSort = <T>(array, prop): T[] => R.sort(R.ascend(R.prop(prop)), array)
export const descSort = <T>(array, prop): T[] => R.sort(R.descend(R.prop(prop)), array)
